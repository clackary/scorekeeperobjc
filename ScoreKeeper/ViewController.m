//
//  ViewController.m
//  ScoreKeeper
//
//  Created by Zachary Cole on 1/19/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    //Initialize UI elements
    labelA = [[UILabel alloc]initWithFrame:CGRectZero];
    labelA.text = @"0";
    labelA.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:labelA];
    
    labelB = [[UILabel alloc]initWithFrame:CGRectZero];
    labelB.text = @"0";
    labelB.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:labelB];
    
    fieldA = [[UITextField alloc]initWithFrame:CGRectZero];
    fieldA.text = @"Enter name here";
    fieldA.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:fieldA];
    
    fieldB = [[UITextField alloc]initWithFrame:CGRectZero];
    fieldB.text = @"Enter name here";
    fieldB.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:fieldB];
    
    stepperA = [[UIStepper alloc]initWithFrame:CGRectZero];
    [stepperA addTarget:self action:@selector (stepperAAction:) forControlEvents:UIControlEventValueChanged];
    stepperA.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:stepperA];
    
    stepperB = [[UIStepper alloc]initWithFrame:CGRectZero];
    [stepperB addTarget:self action:@selector (stepperBAction:) forControlEvents:UIControlEventValueChanged];
    stepperB.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:stepperB];
    
    reset = [UIButton buttonWithType:UIButtonTypeSystem];
    [reset setTitle:@"Reset" forState:UIControlStateNormal];
    reset.frame = CGRectZero;
    reset.translatesAutoresizingMaskIntoConstraints = NO;
    [reset addTarget:self action:@selector(resetAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:reset];
    
    [fieldA setDelegate: (id)self];
    [fieldB setDelegate: (id)self];
    
    //Layout
    NSDictionary* variableDictionary = NSDictionaryOfVariableBindings(labelA, labelB, fieldA, fieldB, stepperA, stepperB, reset);
    
    NSDictionary* metrics = @{};
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-50-[labelA]-[labelB]-50-|" options:0 metrics:metrics views:variableDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-50-[fieldA]" options:0 metrics:metrics views:variableDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[fieldB]-50-|" options:0 metrics:metrics views:variableDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-50-[stepperA]" options:0 metrics:metrics views:variableDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[stepperB]-50-|" options:0 metrics:metrics views:variableDictionary]];
    
    //Why does adding a horizontal limit(50) break this constraint?
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[reset]-|" options:0 metrics:metrics views:variableDictionary]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[fieldA(50)]-[labelA(100)]-[stepperA]" options:0 metrics:metrics views:variableDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[fieldB(50)]-[labelB(100)]-[stepperB]" options:0 metrics:metrics views:variableDictionary]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[reset(50)]-200-|" options:0 metrics:metrics views:variableDictionary]];
}

-(void)stepperAAction:(UIStepper*)stepper {
    labelA.text = @(stepper.value).stringValue;
    [self.view endEditing:YES];
}

-(void)stepperBAction:(UIStepper*)stepper {
    labelB.text = @(stepper.value).stringValue;
    [self.view endEditing:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField {
    [textField resignFirstResponder];
    return YES;
}

// Resets both stepper values to zero.
// Is there a way to statically call stepper action functions?
-(void)resetAction:(UIButton*)button {
    stepperA.value = 0;
    stepperB.value = 0;
    labelA.text = @"0";
    labelB.text = @"0";
    fieldA.text = @"Enter name here";
    fieldB.text = @"Enter name here";
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
