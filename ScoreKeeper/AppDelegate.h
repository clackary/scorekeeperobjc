//
//  AppDelegate.h
//  ScoreKeeper
//
//  Created by Zachary Cole on 1/19/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

