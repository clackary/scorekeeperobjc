//
//  ViewController.h
//  ScoreKeeper
//
//  Created by Zachary Cole on 1/19/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    UILabel* labelA;
    UILabel* labelB;
    UITextField* fieldA;
    UITextField* fieldB;
    UIStepper* stepperA;
    UIStepper* stepperB;
    UIButton* reset;
    
    
}

@end

